CREATE DATABASE IF NOT EXISTS testecliente;
USE testecliente;
create TABLE IF NOT EXISTS clientes (
	id INT(11) AUTO_INCREMENT,
	nome_cliente VARCHAR(255) NOT NULL ,
	cpf_cliente VARCHAR(11) NOT NULL,
	data_nascimento_cliente DATETIME UNIQUE NOT NULL,
	PRIMARY KEY (id)
)

	