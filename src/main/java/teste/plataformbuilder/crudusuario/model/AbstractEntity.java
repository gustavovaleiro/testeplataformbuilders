package teste.plataformbuilder.crudusuario.model;

public abstract class AbstractEntity<R> {
	
	public abstract R getId();
	public abstract void setId(R id);
	
}
