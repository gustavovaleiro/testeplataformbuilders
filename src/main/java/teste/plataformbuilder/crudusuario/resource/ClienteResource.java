package teste.plataformbuilder.crudusuario.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import teste.plataformbuilder.crudusuario.model.Cliente;
import teste.plataformbuilder.crudusuario.service.AbstractRestService;
import teste.plataformbuilder.crudusuario.service.ClienteService;
@RestController
@RequestMapping(value = "/clientes")
public class ClienteResource extends AbstractRestController<Integer, Cliente> {

	@Autowired
	private ClienteService clienteService;
	
	
	@PatchMapping(value = "/{id}")
	public ResponseEntity<Void> patchCliente(@RequestBody Cliente cliente, @PathVariable("id") Integer id){
		cliente.setId(id);
		this.clienteService.patch(cliente);
		
		return ResponseEntity.noContent().build();
		
	}
	@GetMapping(value = "/page")
	public ResponseEntity<Page<Cliente>> findAllBy(
			@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "cpf", required = false) String cpf,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "10") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "id") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction){
				PageRequest pageRequest = PageRequest.of(page, linesPerPage,
						Direction.valueOf(direction), orderBy);
				Page<Cliente> rs = clienteService.findBy(nome, cpf,pageRequest); 
				return ResponseEntity.ok().body(rs);   
	}
	@Override
	public AbstractRestService<Integer, Cliente> getService() {
		return this.clienteService;
	}

}
