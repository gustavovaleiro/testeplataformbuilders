package teste.plataformbuilder.crudusuario.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import teste.plataformbuilder.crudusuario.model.Cliente;
import teste.plataformbuilder.crudusuario.repository.ClienteRepository;
@Service
public class ClienteService extends AbstractRestService<Integer, Cliente>{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public boolean validateEntityForSave(Cliente entity) {
		return true;
	}

	@Override
	public JpaRepository<Cliente, Integer> getRepository() {
		return clienteRepository;
	}

	public void patch(Cliente cliente) {
		Cliente clienteSaved = this.getById(cliente.getId());
		if(!StringUtils.isEmpty(cliente.getNome())) {
			clienteSaved.setNome(cliente.getNome());
		}
		if(!StringUtils.isEmpty(cliente.getCpf())) {
			clienteSaved.setCpf(cliente.getCpf());
		}
		if(cliente.getDataNascimento() != null) {
			clienteSaved.setDataNascimento(cliente.getDataNascimento());
		}
		
		this.clienteRepository.save(clienteSaved);
	}

	public Page<Cliente> findBy(String nome, String cpf, PageRequest pageRequest) {
		Cliente clienteExample = new Cliente();
		clienteExample.setNome(nome);
		clienteExample.setCpf(cpf);
		ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
		return clienteRepository.findAll(Example.of(clienteExample,matcher), pageRequest);
	}

}
