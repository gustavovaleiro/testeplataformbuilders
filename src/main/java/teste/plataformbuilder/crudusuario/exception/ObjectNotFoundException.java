package teste.plataformbuilder.crudusuario.exception;

public class ObjectNotFoundException extends RuntimeException
{
	private static final long serialVersionUID = 1L;
	public static final String DEFAULT = "Erro de integridade dos dados";
	
	public ObjectNotFoundException(String msg) {
		super(msg);
	}
	
	public ObjectNotFoundException(String msg, Throwable cause) {
		super(msg,cause);
	}
}

